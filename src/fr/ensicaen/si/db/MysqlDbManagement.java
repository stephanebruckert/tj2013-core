package fr.ensicaen.si.db;

import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MysqlDbManagement extends ADbManagement {

	final static String NomBase = "jdbc:mysql://localhost:8889/si";
	final static String User = "root";
	final static String Password = "root";
	final static String clazz = "com.mysql.jdbc.Driver";
	final static String driverName = "MySQL JDBC Driver";

	@Override
	public boolean connection() throws SQLException {
		System.out.println("-------- MySQL JDBC Connection Testing ------------");

		try {
			Class.forName(clazz);
		} catch (ClassNotFoundException e) {
			System.out.println("Where is your MySQL JDBC Driver?");
			e.printStackTrace();
			return false;
		}

		System.out.println("MySQL JDBC Driver Registered!");

		try {
			connection = DriverManager.getConnection(NomBase, User, Password);
		} catch (SQLException e) {
			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return false;
		}

		if (connection != null) {
			System.out.println("You made it, take control your database now!");
		} else {
			System.out.println("Failed to make connection!");
		}
		return false;
	}

	@Override
	public boolean deconnection() throws SQLException {
		if (connection.isValid(10)) {
			connection.close();
			return true;
		}
		return false;
	}

	@Override
	public ResultSet query(String query) throws SQLException {
		DatabaseMetaData md = connection.getMetaData();
		ResultSet rs = md.getTables(null, null, "%", null);

		while (rs.next()) {
		  System.out.println(rs.getString(3));
		}

		return connection.createStatement().executeQuery(query);
	}

}