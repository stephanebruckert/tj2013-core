package fr.ensicaen.si.db;

import java.sql.ResultSet;
import java.sql.SQLException;
 
public class DbManagement implements IDbManagement {
	
	private static DbManagement instance = null;
	private IDbManagement delegate = null;
	
	public static DbManagement getInstance() {
		if (instance == null) {
			instance = new DbManagement();
		}
		return instance;
	}
	
	public void setDelegate(IDbManagement mgr) {
		delegate = mgr;
	}
	
	public boolean isDelegated() {
		return !(delegate == null);
	}
	
	public boolean connection() throws SQLException {
		if (isDelegated()) {
			return delegate.connection();
		} else {
			System.err.println("No database type selected");
		}
		return false;
	}
	
	public boolean deconnection() throws SQLException {
		if (isDelegated()) {
			return delegate.deconnection();
		} else {
			System.err.println("No database type selected");
		}
		return false;
	}
	
	public ResultSet query(String query) throws SQLException {
		if (isDelegated()) {
			return delegate.query(query);
		} else {
			System.err.println("No database type selected");
		}
		return null;
	}
}
