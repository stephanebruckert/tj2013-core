package fr.ensicaen.si.db;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface IDbManagement {
	
	public boolean connection() throws SQLException;
	public boolean deconnection() throws SQLException;
	public ResultSet query(String query) throws SQLException;
	
}
