package fr.ensicaen.si.dao.client;

import java.util.ArrayList;
import java.util.List;
import fr.ensicaen.si.model.*;

public abstract class AClientDao implements IClientDao {
	
	protected List<Client> clients;
	
	public AClientDao() {
		this.clients = new ArrayList<Client>();
	}
}
