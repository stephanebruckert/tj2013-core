package fr.ensicaen.si.dao.client;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.ensicaen.si.db.DbManagement;
import fr.ensicaen.si.db.MysqlDbManagement;
import fr.ensicaen.si.model.Client;

public class DbClientDao extends AClientDao {

	public DbClientDao() {
		DbManagement dbMgr = DbManagement.getInstance();
		dbMgr.setDelegate(new MysqlDbManagement());
		try {
			dbMgr.connection();
			updateClientList();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private List<Client> updateClientList() {
		try {
			ResultSet rs = DbManagement.getInstance().query("select * from tabclient");
			clients.clear();
			Client c;
			while (rs.next()) {
				int id = rs.getInt("idNumClient");
				String first = rs.getString("txtPrenomCli");
				String last = rs.getString("txtNomCli");
				c = new Client(id, last, first);
				clients.add(c);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return clients;
	}
	
	@Override
	public int countClient() {
		return clients.size();
	}

	@Override
	public List<Client> getClients() {
		return clients;
	}

	@Override
	public List<Client> getByName(String name) {
		List<Client> requestedClients = new ArrayList<Client>();
		for (Client c : clients) {
			if (c.getLast().startsWith(name)) {
				requestedClients.add(c);
			}
		}
		return requestedClients;
	}

	@Override
	public List<Client> getByFullName(String name, String firstName) {
		System.out.println("name" + name) ;
		List<Client> requestedClients = new ArrayList<Client>();
		for (Client c : clients) {
			if (c.getLast().startsWith(name) && c.getFirst().startsWith(firstName)) {
				requestedClients.add(c);
			}
		}
		return requestedClients;
	}

	@Override
	public Client getById(int id) {
		for (Client c : clients) {
			if (c.getId() == id) {
				return c;
			}
		}
		return null;
	}

}
