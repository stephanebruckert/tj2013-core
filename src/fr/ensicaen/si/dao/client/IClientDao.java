package fr.ensicaen.si.dao.client;

import java.util.List;

import fr.ensicaen.si.model.Client;

public interface IClientDao {
	
	public int countClient();
	public List<Client> getClients();
	public List<Client> getByName(String name);
	public List<Client> getByFullName(String name, String firstName);
	public Client getById(int id);
	
}
