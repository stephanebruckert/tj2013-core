package fr.ensicaen.si.dao.operation;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.ensicaen.si.dao.client.ClientDao;
import fr.ensicaen.si.dao.client.DbClientDao;
import fr.ensicaen.si.db.DbManagement;
import fr.ensicaen.si.db.MysqlDbManagement;
import fr.ensicaen.si.model.Client;
import fr.ensicaen.si.model.Operation;
import fr.ensicaen.si.model.Result;

public class DbOperationDao implements IOperationDao {

	private DbManagement dbMgr;
	
	public DbOperationDao() {
		dbMgr = DbManagement.getInstance();
		dbMgr.setDelegate(new MysqlDbManagement());
		try {
			dbMgr.connection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private List<Operation> getOperationListFromQuery(String query) {
		List<Operation> operations = new ArrayList<Operation>();
		try {
			ResultSet rs = DbManagement.getInstance().query(query);
			Operation c;
			while (rs.next()) {
				int id = rs.getInt("idNumOperation");
				String cardNum = rs.getString("idNumCarte");
				String accountNum = rs.getString("idNumCompte");
				float amount = rs.getFloat("numMontantOpe");
				String date = rs.getString("datOpe");
				c = new Operation(id, cardNum, accountNum, amount, date);
				operations.add(c);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return operations;
	}
	
	@Override
	public List<Operation> getById(int id) {
		return getOperationListFromQuery(
				"SELECT ope.* "+
				   "FROM taboperation ope "+
				   "INNER JOIN relclientcompte cc ON cc.idNumCompte = ope.idNumCompte "+
				   "INNER JOIN tabclient cl ON cl.idNumClient = cc.idNumClient "+
				   "WHERE cl.idNumClient = '"+id+"' UNION "+
				"SELECT ope.* "+
				   "FROM taboperation ope "+
				   "INNER JOIN tabcarte ca ON ca.idNumCarte = ope.idNumCarte "+
				   "INNER JOIN tabclient cl ON cl.idNumClient = ca.idNumClient "+
				   "WHERE cl.idNumClient = '"+id+"'");
	}

	@Override
	public Result getByName(String lastName) {
		Result result = new Result();
		
		ClientDao cd = ClientDao.getInstance();
		cd.setDelegate(new DbClientDao());
		
		result.setClients(cd.getByName(lastName));
		if ((result.getClients()).size() == 1) {
			result.setOperations(this.getById(
					result.getClients().get(0).getId()));
		}
		
		return result;
	}

	@Override
	public Result getByFullName(String lastName, String firstName) {
		Result result = new Result();
		
		ClientDao cd = ClientDao.getInstance();
		List<Client> requestedClients = cd.getByFullName(lastName, firstName);
		result.setClients(requestedClients);

		if ((result.getClients()).size() == 1) {
			List<Operation> currentOperations = this.getById(result.getClients().get(0).getId());
			result.setOperations(currentOperations);
		}
		
		return result;
	}

}
