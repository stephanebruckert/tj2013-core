package fr.ensicaen.si.dao.operation;

import java.util.List;

import fr.ensicaen.si.model.Operation;
import fr.ensicaen.si.model.Result;

public class OperationDao implements IOperationDao{
	
	private static OperationDao instance;
	
	private IOperationDao delegate;
 
	public static OperationDao getInstance() {
		if (instance == null)
			instance = new OperationDao();
		return instance;
	}
 	 
	public void setDelegate(IOperationDao delegate) {
		this.delegate = delegate;
	}
	 
	public boolean isDelegated() {
		 return !(delegate == null);
	}

	@Override
	public List<Operation> getById(int id) {
		return delegate.getById(id);
	}

	@Override
	public Result getByFullName(String lastName, String firstName) {
		return delegate.getByFullName(lastName, firstName);
	}

	@Override
	public Result getByName(String lastName) {
		return delegate.getByName(lastName);
	}

}
