package fr.ensicaen.si.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Client {

	private int id;
	private String last;
	private String first;
	
	public Client() {
		
	}
	
	public Client(int id, String last, String first) {
		this.id = id;
		this.last = last;
		this.first = first;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLast() {
		return last;
	}

	public void setLast(String last) {
		this.last = last;
	}

	public String getFirst() {
		return first;
	}

	public void setFirst(String first) {
		this.first = first;
	}

	@Override
	public String toString() {
		return "Client [id=" + id + ", last=" + last + ", first=" + first + "]";
	}
	
}
