package fr.ensicaen.si.model;

import java.util.ArrayList;
import java.util.List;

public class Result {

	private List<Client> clients;
	private List<Operation> operations;
	
	public Result() {
		this.clients = new ArrayList<Client>();
		this.operations = new ArrayList<Operation>();
	}
	
	public List<Client> getClients() {
		return clients;
	}
	
	public void setClients(List<Client> clients) {
		this.clients = clients;
	}
	
	public List<Operation> getOperations() {
		return operations;
	}
	
	public void setOperations(List<Operation> operations) {
		this.operations = operations;
	}
	
	public String toString() {
		return this.clients.toString() + "\n"
				+ this.operations.toString();
	}
	
}
