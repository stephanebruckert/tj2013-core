package fr.ensicaen.si.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Operation {

	private int id;
	private String cardNum;
	private String accountNum;
	private float amount;
	private String date;
	
	public Operation() {
		
	}
	
	public Operation(int id, String cardNum, String accountNum, float amount,
			String date) {
		super();
		this.id = id;
		this.cardNum = cardNum;
		this.accountNum = accountNum;
		this.amount = amount;
		this.date = date;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCardNum() {
		return cardNum;
	}

	public void setCardNum(String cardNum) {
		this.cardNum = cardNum;
	}

	public String getAccountNum() {
		return accountNum;
	}

	public void setAccountNum(String accountNum) {
		this.accountNum = accountNum;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

}
